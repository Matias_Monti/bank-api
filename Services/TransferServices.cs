﻿using AutoMapper;
using Bank_API.Data;
using Bank_API.Integration;
using Bank_API.Models;
using Bank_API.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank_API.Services
{
    public class TransferServices : ITransferServices
    {
        private readonly Bank_APIContext Context;
        private readonly IMapper Mapper;
        private readonly int IDLoggedUser;

        public TransferServices(Bank_APIContext cContext, IMapper cMapper)
        {
            this.Context = cContext;
            this.Mapper = cMapper;
            IDLoggedUser = 1;//hardcoded
        }

        public async Task InsertAsync(TransferModel transferModel)
        {
            Account senderAccount = await Context.Account.Where(rec => rec.accountNumber == transferModel.accountFrom).FirstOrDefaultAsync();

            Account receiverAccount = await Context.Account.Where(rec => rec.accountNumber == transferModel.accountTo).FirstOrDefaultAsync();

            Validate(transferModel, senderAccount, receiverAccount);

            await this.UpdateAccountsAsync(transferModel, senderAccount, receiverAccount);

            await this.InsertTransferAsync(transferModel);
        }

        private void Validate(TransferModel transfer, Account senderAccount, Account receiverAccount)
        {
            if (senderAccount.userId != IDLoggedUser)
            {
                throw new Exception("El usuario que realiza la consulta no es el mismo que esta logueado");
            }

            if (senderAccount == null || receiverAccount == null)
            {
                throw new Exception("Deben existir ambas cuentas bancarias");
            }

            if (senderAccount.amount < transfer.amount)
            {
                throw new Exception("El remitente no posee dinero suficiente para la transferencia");
            }
        }

        private async Task UpdateAccountsAsync(TransferModel transferModel, Account senderAccount, Account receiverAccount)
        {
            senderAccount.amount -= transferModel.amount;

            decimal conversion = await this.GetConversionAsync(senderAccount.currencyId, receiverAccount.currencyId);

            receiverAccount.amount += transferModel.amount / conversion;
        }

        private async Task<decimal> GetConversionAsync(int senderCurrencyID, int recieverCurrencyID)
        {
            decimal conversion = 1;

            if (senderCurrencyID != recieverCurrencyID)
            {
                Currency senderCurrency = await Context.Currency.Where(rec => rec.id == senderCurrencyID).FirstOrDefaultAsync();

                Currency recieverCurrency = await Context.Currency.Where(rec => rec.id == recieverCurrencyID).FirstOrDefaultAsync();

                if (senderCurrency.latestConversion < System.DateTime.Today || recieverCurrency.latestConversion < System.DateTime.Today)
                {
                    await this.GetCurrencys(senderCurrency, recieverCurrency);
                }

                conversion = senderCurrency.exchange / recieverCurrency.exchange;
            }

            return conversion;
        }

        private async Task GetCurrencys(Currency senderCurrency, Currency recieverCurrency)
        {
            FixerCurrencyAPI cCurrencyApi = new FixerCurrencyAPI(Context);

            ExcahngeResult updatedCurrencys = await cCurrencyApi.GetUpdatedExchangeAsync(senderCurrency, recieverCurrency);

            foreach (KeyValuePair<string, decimal> currencyExchange in updatedCurrencys.rates)
            {
                Currency actualCurrency = await Context.Currency.Where(rec => rec.currency == currencyExchange.Key).FirstOrDefaultAsync();

                actualCurrency.exchange = currencyExchange.Value;

                actualCurrency.latestConversion = updatedCurrencys.date;

                if (senderCurrency.currency == currencyExchange.Key) { senderCurrency.exchange = currencyExchange.Value; } 

                if (recieverCurrency.currency == currencyExchange.Key) { recieverCurrency.exchange = currencyExchange.Value; } 
            }
        }

        private async Task InsertTransferAsync(TransferModel transferModel)
        {
            Context.Transfer.Add(this.Mapper.Map<TransferModel, Transfer>(transferModel));

            await Context.SaveChangesAsync();
        }
    }

    public interface ITransferServices
    {
        public Task InsertAsync(TransferModel transfer);
    }
}
