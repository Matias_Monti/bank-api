﻿using AutoMapper;
using Bank_API.Data;
using Bank_API.Models;
using Bank_API.Models.Transaction;
using Bank_API.Tables;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank_API.Services
{
    public class TransactionsServices : ITransactionsServices
    {
        private readonly Bank_APIContext Context;
        private readonly IMapper Mapper;
        private readonly int IDLoggedUser;

        public TransactionsServices(Bank_APIContext cContext, IMapper cMapper)
        {
            this.Context = cContext;
            this.Mapper = cMapper;
            IDLoggedUser = 1;//hardcoded
        }

        public async Task<List<TransferModel>> GetTransactionsAsync(TransactionFilter cTransactionFilter)
        {
            Validate(cTransactionFilter.UserID);

            List<int> accountNumbers = await Context.Account.Where(rec => rec.userId == cTransactionFilter.UserID).Select(rec => rec.accountNumber).ToListAsync();

            IQueryable<Transfer> lisTransactions = Context.Transfer.Where(rec => accountNumbers.Contains(rec.accountFrom) ||
                                                                                 accountNumbers.Contains(rec.accountTo));

            FilterTransactions(cTransactionFilter, ref lisTransactions);

            return this.Mapper.Map<List<Transfer>, List<TransferModel>>(await lisTransactions.ToListAsync());
        }

        private void FilterTransactions(TransactionFilter cTransactionFilter, ref IQueryable<Transfer> lisTransactions)
        {
            if (cTransactionFilter.From.HasValue)
            {
                lisTransactions = lisTransactions.Where(rec => rec.date >= cTransactionFilter.From);
            }

            if (cTransactionFilter.To.HasValue)
            {
                lisTransactions = lisTransactions.Where(rec => rec.date <= cTransactionFilter.To.Value.Date.AddDays(1).AddTicks(-1));
            }

            if (cTransactionFilter.SourceAccountID > 0)
            {
                lisTransactions = lisTransactions.Where(rec => rec.accountFrom == cTransactionFilter.SourceAccountID || rec.accountTo == cTransactionFilter.SourceAccountID);
            }
        }

        private void Validate(int userID)
        {
            if (userID != IDLoggedUser)
            {
                throw new Exception("El usuario que realiza la consulta no es el mismo que esta logueado");
            }
        }
    }

    public interface ITransactionsServices
    {
        public Task<List<TransferModel>> GetTransactionsAsync(TransactionFilter cTransactionFilter);
    }
}
