﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank_API.Tables
{
    public class Transfer
    {
        public int id { get; set; }
        public int accountFrom { get; set; }
        public int accountTo { get; set; }
        public decimal amount { get; set; }
        public DateTime? date { get; set; }
        public String description { get; set; }
    }
}