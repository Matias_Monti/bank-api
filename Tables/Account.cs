﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank_API.Tables
{
    public class Account
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int currencyId { get; set; }
        public int accountNumber { get; set; }
        public decimal amount { get; set; }
    }
}