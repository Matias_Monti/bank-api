﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank_API.Tables
{
    public class User
    {
        public int id { get; set; }
        public String name { get; set; }
        public String password { get; set; }
    }
}
