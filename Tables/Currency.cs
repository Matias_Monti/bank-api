﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank_API.Tables
{
    public class Currency
    {
        public int id { get; set; }
        public String currency { get; set; }
        public decimal exchange { get; set; }
        public DateTime? latestConversion { get; set; }
        public bool isDefault { get; set; }
    }
}
