﻿using Bank_API.Models.Transaction;
using Bank_API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Bank_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {

        private readonly ITransactionsServices TransactionsServices;

        public TransactionsController(ITransactionsServices cTransactionsServices)
        {
            this.TransactionsServices = cTransactionsServices;
        }

        [HttpGet]
        public async Task<IActionResult> Transactions([FromQuery] TransactionFilter cTransactionFilter)
        {
            return Ok(await this.TransactionsServices.GetTransactionsAsync(cTransactionFilter));
        }
    }
}
