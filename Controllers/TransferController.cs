﻿using Bank_API.Models;
using Bank_API.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bank_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransferController : ControllerBase
    {
        private readonly ITransferServices TransferServices;
        public TransferController(ITransferServices cTransferServices)
        {
            this.TransferServices = cTransferServices;
        }

        [HttpPost]
        public async Task<IActionResult> Transfer(TransferModel cTransferModel)
        {
            await this.TransferServices.InsertAsync(cTransferModel);
            return Ok();
        }
    }
}
