﻿using Bank_API.Models;
using Bank_API.Tables;
using System.Threading.Tasks;

namespace Bank_API.Integration
{
    public interface ICurrencyAPI
    {
        Task<ExcahngeResult> GetUpdatedExchangeAsync(Currency senderCurrency, Currency recieverCurrency);
    }
}