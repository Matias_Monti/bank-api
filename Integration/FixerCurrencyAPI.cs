﻿using Bank_API.Data;
using Bank_API.Models;
using Bank_API.Tables;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Bank_API.Integration
{
    public class FixerCurrencyAPI : ICurrencyAPI
    {
        private readonly Bank_APIContext Context;
        private string http;
        private string comma;
        private string lisCurrencyCodes;

        public FixerCurrencyAPI(Bank_APIContext cContext)
        {
            this.Context = cContext;
            this.http = "https://api.apilayer.com/fixer/latest?symbols=";
            this.comma = "%2C";
        }

        public async Task<ExcahngeResult> GetUpdatedExchangeAsync(Currency senderCurrency, Currency recieverCurrency)
        {
            string defaultCurrencyCode = await Context.Currency.Where(rec => rec.isDefault == true).Select(rec => rec.currency).FirstOrDefaultAsync();

            List<string> lisCurrencys = await Context.Currency.Where(rec => rec.isDefault != true).Select(rec => rec.currency).ToListAsync();

            lisCurrencyCodes += defaultCurrencyCode;

            foreach (string cCode in lisCurrencys)
            {
                lisCurrencyCodes += comma + cCode;
            }

            Uri cHttpUri = new Uri($"{http}{lisCurrencyCodes}&base={defaultCurrencyCode}");

            HttpClient cHttpClient = new HttpClient();

            cHttpClient.DefaultRequestHeaders.Add("apikey", "KmPGwHYU0oSE0knHWTBKOuo30zGz27Of");

            HttpResponseMessage cHttpResponseMessage = await cHttpClient.GetAsync(cHttpUri);

            ExcahngeResult cExcahngeResult = new ExcahngeResult();

            if (cHttpResponseMessage.IsSuccessStatusCode)
            {
                string strRespuesta = await cHttpResponseMessage.Content.ReadAsStringAsync();

                cExcahngeResult = JsonConvert.DeserializeObject<ExcahngeResult>(strRespuesta);
            }

            else
            {
                cExcahngeResult.IsError = true;

                cExcahngeResult.Error = cHttpResponseMessage.ReasonPhrase; 
                
                throw new Exception("No se pudo obtener la cotizacion del dia: " + cExcahngeResult.Error);
            }

            return cExcahngeResult; ;
        }
    }
}
