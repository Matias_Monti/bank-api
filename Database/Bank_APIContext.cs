﻿using Bank_API.Tables;
using Microsoft.EntityFrameworkCore;

namespace Bank_API.Data
{
    public class Bank_APIContext : DbContext
    {
        public Bank_APIContext (DbContextOptions<Bank_APIContext> options)
            : base(options)
        {
        }

        public DbSet<Transfer> Transfer { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Currency> Currency { get; set; }
    }
}
