﻿CREATE DATABASE BANK
GO

USE BANK 
GO


CREATE TABLE [dbo].[Account] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [userId]        INT             NOT NULL,
    [currencyId]    INT             NOT NULL,
    [accountNumber] INT             NOT NULL,
    [amount]        DECIMAL (18, 2) NULL
);

CREATE TABLE [dbo].[Currency] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [isDefault]        BIT             NULL,
    [exchange]         DECIMAL (18, 5) NOT NULL,
    [latestConversion] DATETIME2 (7)   NOT NULL,
    [currency]         VARCHAR (3)     NOT NULL
);

CREATE TABLE [dbo].[Transfer] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [accountFrom] INT             NULL,
    [accountTo]   INT             NULL,
    [amount]      DECIMAL (18, 2) NULL,
    [date]        DATETIME2 (7)   NULL,
    [description] VARCHAR (50)    NULL
);

CREATE TABLE [dbo].[User] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [name]     VARCHAR (50) NULL,
    [password] VARCHAR (50) NULL
);

--INSERTS INITIAL DATA
INSERT INTO [dbo].[User] (name) VALUES ('User1'),('User2')
GO

INSERT INTO CURRENCY (CURRENCY,EXCHANGE,ISDEFAULT,LATESTCONVERSION) VALUES ('USD', 1.2, FALSE, '2023-01-01'),('EUR', 1.2, FALSE, '2023-01-01'),('UYU', 1, TRUE, '2023-01-01')
GO

INSERT INTO ACCOUNT (USERID,CURRENCYID,ACCOUNTNUMBER,AMOUNT) VALUES (1, 1, 777, 100),(1, 2, 888, 100),(2, 2, 999, 100)
GO

INSERT INTO [dbo].[Transfer] (ACCOUNTFROM,ACCOUNTTO,AMOUNT,DESCRIPTION,DATE) VALUES (777, 888, 10, 'transfer1', '2023-01-01'), (888, 999, 15, 'transfer2', '2023-01-01')
GO
