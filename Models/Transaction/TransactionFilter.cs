﻿using System;

namespace Bank_API.Models.Transaction
{
    public class TransactionFilter
    {
        public int UserID { get; set; }
        public int SourceAccountID { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}