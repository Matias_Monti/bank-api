﻿using System;

namespace Bank_API.Models
{
    public class TransferModel
    {
        public int accountFrom { get; set; }
        public int accountTo { get; set; }
        public decimal amount { get; set; }
        public DateTime? date { get; set; }
        public String description { get; set; }
    }
}