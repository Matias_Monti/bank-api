﻿using AutoMapper;
using Bank_API.Models;
using Bank_API.Tables;

namespace Fx.Aplicacion.Modelos.ModelosClaseDocumento
{
    public class TransferMapper : Profile
    {
        public TransferMapper()
        {
            CreateMap<Transfer, TransferModel>();

            CreateMap<TransferModel, Transfer>();
        }
    }
}