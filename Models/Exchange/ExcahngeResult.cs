﻿using System;
using System.Collections.Generic;

namespace Bank_API.Models
{
    public class ExcahngeResult
    {
        public DateTime? date { get; set; }
        public string basee { get; set; }
        public string Success { get; set; }
        public string timestamp { get; set; }
        public bool IsError { get; set; }
        public string Error { get; set; }
        public string @base { get; set; }
        public Dictionary<string, decimal> rates { get; set; }
    }
}
