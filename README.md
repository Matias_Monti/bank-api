# BANK API

Aspectos a mejorar del programa:

Tener un token de autorizacion para acceder a los endpoints de la api (seguridad).

Endpoints para grabado y consulta de las tablas como usuario y cuenta.

Establecer un modelo de respuesta para los endpoints donde ademas del mensaje, se pueda enviar datos que sean utiles para el front.

Validadores robustos para cada endpoint (una clase aparte dedicada a validaciones para no agregar codigo extra dentro de los servicios)

Tener un set de mensajes/validaciones guardados (donde hasta podria tener distincion por el idioma del sistema)

Controles sobre lo recibido en la api externa (caso que la api no me devuelva una respuesta positiva)

Clean arqutecture: 
separar el proyecto por capas, ahora las carpetas de controllers estan al mismo nivel que el dominio de base de datos.

Mejorar el sistema de indexado de tablas a nivel base de datos y tambien proyecto, por ejemplo que el objeto dto de personas contenga una lista de cuentas,
lo que a nivel de base de datos y con esto podria controlar que no se cree una cuenta que apunte a un usuario que no existe o eliminar un usuario sin antes eliminar sus cuentas
en sql serian las foreign keys.


